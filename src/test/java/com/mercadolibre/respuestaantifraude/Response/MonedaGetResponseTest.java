package com.mercadolibre.respuestaantifraude.Response;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MonedaGetResponseTest {

    private final MonedaGetResponse monedaGetResponse = new MonedaGetResponse();

    @Test
    void getResult() {
        monedaGetResponse.setResult("3922.5");
        assertEquals("3922.5", monedaGetResponse.getResult());
    }

    @Test
    void setResult() {
        monedaGetResponse.setResult("3922.5");
        assertEquals("3922.5", monedaGetResponse.getResult());
    }

    @Test
    void builder() {
        MonedaGetResponse response = MonedaGetResponse.builder()
                .result("3922.5")
                .build();
        assertEquals("3922.5",response.getResult());
    }
}