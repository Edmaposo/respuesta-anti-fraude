package com.mercadolibre.respuestaantifraude.Response;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class LocalizacionGetResponseTest {

    private final LocalizacionGetResponse localizacionGetResponse = new LocalizacionGetResponse();
    private final LanguajeGetResponse languajeGetResponse = new LanguajeGetResponse("es");
    private final LocationGetResponse locationGetResponse = new LocationGetResponse(new LanguajeGetResponse[] {languajeGetResponse});

    @Test
    void getIp() {
        localizacionGetResponse.setIp("111.111.111.111");
        assertEquals("111.111.111.111", localizacionGetResponse.getIp());
    }

    @Test
    void getType() {
        localizacionGetResponse.setType("ipv4");
        assertEquals("ipv4", localizacionGetResponse.getType());
    }

    @Test
    void getContinent_code() {
        localizacionGetResponse.setContinent_code("SA");
        assertEquals("SA", localizacionGetResponse.getContinent_code());
    }

    @Test
    void getContinent_name() {
        localizacionGetResponse.setContinent_name("South America");
        assertEquals("South America", localizacionGetResponse.getContinent_name());
    }

    @Test
    void getCountry_code() {
        localizacionGetResponse.setCountry_code("CO");
        assertEquals("CO", localizacionGetResponse.getCountry_code());
    }

    @Test
    void getCountry_name() {
        localizacionGetResponse.setCountry_name("Colombia");
        assertEquals("Colombia", localizacionGetResponse.getCountry_name());
    }

    @Test
    void getRegion_code() {
        localizacionGetResponse.setRegion_code("DC");
        assertEquals("DC", localizacionGetResponse.getRegion_code());
    }

    @Test
    void getRegion_name() {
        localizacionGetResponse.setRegion_name("Bogota D.C.");
        assertEquals("Bogota D.C.", localizacionGetResponse.getRegion_name());
    }

    @Test
    void getLocation() {
        localizacionGetResponse.setLocation(locationGetResponse);
        assertEquals(locationGetResponse, localizacionGetResponse.getLocation());
    }

    @Test
    void getCity() {
        localizacionGetResponse.setCity("Bogotá");
        assertEquals("Bogotá", localizacionGetResponse.getCity());
    }

    @Test
    void setIp() {
        localizacionGetResponse.setIp("111.111.111.111");
        assertEquals("111.111.111.111", localizacionGetResponse.getIp());
    }

    @Test
    void setType() {
        localizacionGetResponse.setType("ipv4");
        assertEquals("ipv4", localizacionGetResponse.getType());
    }

    @Test
    void setContinent_code() {
        localizacionGetResponse.setContinent_code("SA");
        assertEquals("SA", localizacionGetResponse.getContinent_code());
    }

    @Test
    void setContinent_name() {
        localizacionGetResponse.setContinent_name("South America");
        assertEquals("South America", localizacionGetResponse.getContinent_name());
    }

    @Test
    void setCountry_code() {
        localizacionGetResponse.setCountry_code("CO");
        assertEquals("CO", localizacionGetResponse.getCountry_code());
    }

    @Test
    void setCountry_name() {
        localizacionGetResponse.setCountry_name("Colombia");
        assertEquals("Colombia", localizacionGetResponse.getCountry_name());
    }

    @Test
    void setRegion_code() {
        localizacionGetResponse.setRegion_code("DC");
        assertEquals("DC", localizacionGetResponse.getRegion_code());
    }

    @Test
    void setRegion_name() {
        localizacionGetResponse.setRegion_name("Bogota D.C.");
        assertEquals("Bogota D.C.", localizacionGetResponse.getRegion_name());
    }

    @Test
    void setCity() {
        localizacionGetResponse.setCity("Bogotá");
        assertEquals("Bogotá", localizacionGetResponse.getCity());
    }

    @Test
    void SetLocation(){
        localizacionGetResponse.setLocation(locationGetResponse);
        assertEquals(locationGetResponse, localizacionGetResponse.getLocation());
    }

    @Test
    void builder() {
        LocalizacionGetResponse response = LocalizacionGetResponse.builder()
                .ip("111.111.111.111")
                .type("ipv4")
                .country_name("Colombia")
                .continent_code("SA")
                .continent_name("South America")
                .country_code("CO")
                .region_code("DC")
                .region_name("Bogota D.C.")
                .city("Bogotá")
                .location(locationGetResponse)
                .build();
        assertEquals("111.111.111.111", response.getIp());
    }
}