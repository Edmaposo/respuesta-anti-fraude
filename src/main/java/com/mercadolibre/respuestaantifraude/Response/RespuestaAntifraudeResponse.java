package com.mercadolibre.respuestaantifraude.Response;

import com.mercadolibre.respuestaantifraude.Response.PaisGetResponse;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RespuestaAntifraudeResponse {
    private String nombrePais;
    private String codigoIso;
    private String moneda;
    private String cotizacionMonedaEnDolares;
}
