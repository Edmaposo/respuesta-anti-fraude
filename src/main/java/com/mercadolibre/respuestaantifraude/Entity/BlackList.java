package com.mercadolibre.respuestaantifraude.Entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "blackList")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BlackList {

    @Id
    private Long id;
    @Column(unique = true)
    private String ip;
}
