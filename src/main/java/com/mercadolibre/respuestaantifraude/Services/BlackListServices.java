package com.mercadolibre.respuestaantifraude.Services;

import com.mercadolibre.respuestaantifraude.Entity.BlackList;
import com.mercadolibre.respuestaantifraude.Repository.BlackLisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlackListServices {

    @Autowired
    private BlackLisRepository blackLisRepository;

    public List<BlackList> getBlackListByIp(String ip){
        return blackLisRepository.findByIp(ip);
    }

    public BlackList addBlackList(String ip){
        BlackList lista = BlackList.builder()
                .id(System.nanoTime())
                .ip(ip)
                .build();
        return blackLisRepository.save(lista);
    }
}
