package com.mercadolibre.respuestaantifraude.Response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LanguajeGetResponse {
    private String code;
}
