package com.mercadolibre.respuestaantifraude;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "respuesta-anti-fraude-rest-api-config")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConfigInfoIp {
    private String keyLocalizacion;
    private String urlLocalicacion;
    private String keyInfoPais;
    private String urlInfoPais;
    private String keyInfoMonedas;
    private String urlInfoMonedas;
}
