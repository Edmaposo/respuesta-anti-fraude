package com.mercadolibre.respuestaantifraude.Controller;

import com.mercadolibre.respuestaantifraude.Entity.BlackList;
import com.mercadolibre.respuestaantifraude.Repository.BlackLisRepository;
import com.mercadolibre.respuestaantifraude.Response.*;
import com.mercadolibre.respuestaantifraude.Services.BlackListServices;
import com.mercadolibre.respuestaantifraude.Services.InfoIpService;
import com.mercadolibre.respuestaantifraude.Services.InfoMonedaService;
import com.mercadolibre.respuestaantifraude.Services.InfoPaisService;
import com.mercadolibre.respuestaantifraude.Utils.UtilTest;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@WebMvcTest(GetInfoIpController.class)
class GetInfoIpControllerTest {

    @MockBean
    private InfoIpService infoIpService;
    @MockBean
    private InfoPaisService infoPaisService;
    @MockBean
    private BlackListServices blackListServices;
    @MockBean
    private InfoMonedaService infoMonedaService;
    @MockBean
    private BlackLisRepository blackLisRepository;
    @MockBean
    private RespuestaAntifraudeResponse respuestaAntifraudeResponse;
    @Autowired
    GetInfoIpController getInfoIpController;


    @Test
    void getInfoIP() throws Exception {
        Mockito.when(blackListServices.getBlackListByIp("111.111.111.111")).thenReturn(new ArrayList<>());
        Mockito.when(infoIpService.getInfoIp(Mockito.any())).thenReturn(UtilTest.getLocalizacionValidRequest());
        Mockito.when(infoPaisService.getInfoPais(Mockito.any())).thenReturn(new PaisGetResponse[]{UtilTest.getPaisValidRequest()});
        Mockito.when(infoMonedaService.getInfoMoneda("COP")).thenReturn(UtilTest.getMonedaValidRequest());

        ResponseEntity<RespuestaAntifraudeResponse> response = getInfoIpController.getInfoIP("111.111.111.111");
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void getInfoIPForbidden() throws Exception {
        Mockito.when(blackListServices.getBlackListByIp("111.111.111.111")).thenReturn(Collections.singletonList(UtilTest.getValidBlackListRequest()));

        ResponseStatusException exc = assertThrows(ResponseStatusException.class, () -> {
            getInfoIpController.getInfoIP("111.111.111.111");
        });
        assertEquals("Sin permisos para visualizar la información", exc.getReason());
        assertEquals(HttpStatus.FORBIDDEN, exc.getStatus());
    }


    @Test
    void postBlackList() throws Exception {
        Mockito.when(blackListServices.getBlackListByIp("111.111.111.111")).thenReturn(new ArrayList<>());
        Mockito.when(blackListServices.addBlackList("111.111.111.111")).thenReturn(UtilTest.getValidBlackListRequest());

        ResponseEntity<BlackList> response = getInfoIpController.postBlackList(UtilTest.getBlackListRequest());
        assertEquals("111.111.111.111", response.getBody().getIp());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void postBlackListBadRequest() {
        Mockito.when(blackListServices.getBlackListByIp("111.111.111.111")).thenReturn(Collections.singletonList(UtilTest.getValidBlackListRequest()));

        ResponseStatusException exc = assertThrows(ResponseStatusException.class, () -> {
            getInfoIpController.postBlackList(UtilTest.getBlackListRequest());
        });
        assertEquals("La IP ya se encuentra registrada", exc.getReason());
        assertEquals(HttpStatus.BAD_REQUEST, exc.getStatus());
    }
}