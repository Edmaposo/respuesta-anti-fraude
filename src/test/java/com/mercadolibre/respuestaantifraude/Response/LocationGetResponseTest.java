package com.mercadolibre.respuestaantifraude.Response;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class LocationGetResponseTest {

    private final LocationGetResponse locationGetResponse = new LocationGetResponse();
    private final LanguajeGetResponse languajeGetResponse = new LanguajeGetResponse("es");

    @Test
    void getLanguages() {
        locationGetResponse.setLanguages(new LanguajeGetResponse[] {languajeGetResponse});
        assertEquals(new LanguajeGetResponse[] {languajeGetResponse}[0], locationGetResponse.getLanguages()[0]);
    }

    @Test
    void setLanguages() {
        locationGetResponse.setLanguages(new LanguajeGetResponse[] {languajeGetResponse});
        assertEquals(new LanguajeGetResponse[] {languajeGetResponse}[0], locationGetResponse.getLanguages()[0]);
    }

    @Test
    void builder() {
        LocationGetResponse response = LocationGetResponse.builder()
                .languages(new LanguajeGetResponse[] {languajeGetResponse})
                .build();
        assertEquals(new LanguajeGetResponse[] {languajeGetResponse}[0], response.getLanguages()[0]);
    }
}