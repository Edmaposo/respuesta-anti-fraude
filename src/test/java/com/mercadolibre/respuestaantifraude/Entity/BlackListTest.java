package com.mercadolibre.respuestaantifraude.Entity;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class BlackListTest {


    private final BlackList  blackList = new BlackList();

    @Test
    void getId() {
        blackList.setId(46856L);
        assertEquals(46856L, blackList.getId());
    }

    @Test
    void getIp() {
        blackList.setIp("111.111.111.111");
        assertEquals("111.111.111.111", blackList.getIp());
    }

    @Test
    void setId() {
        blackList.setId(46856L);
        assertEquals(46856L, (long) blackList.getId());
    }

    @Test
    void setIp() {
        blackList.setIp("111.111.111.111");
        assertEquals("111.111.111.111", blackList.getIp());
    }

    @Test
    void builder() {
        BlackList response = BlackList.builder()
                .id(46856L)
                .ip("111.111.111.111")
                .build();
        assertEquals("111.111.111.111", response.getIp());
    }
}