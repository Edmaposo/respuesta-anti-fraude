package com.mercadolibre.respuestaantifraude.Services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercadolibre.respuestaantifraude.ConfigInfoIp;
import com.mercadolibre.respuestaantifraude.Response.LanguajeGetResponse;
import com.mercadolibre.respuestaantifraude.Response.LocalizacionGetResponse;
import com.mercadolibre.respuestaantifraude.Response.LocationGetResponse;
import mockwebserver3.MockResponse;
import mockwebserver3.MockWebServer;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.IOException;

class InfoIpServiceTest {
    @MockBean
    private InfoIpService infoIpService;
    private static MockWebServer mockWebServer;

    @BeforeAll
    static void setUp() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
    }

    @AfterAll
    static void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

    @BeforeEach
    void initialize() {
        ConfigInfoIp configInfoIp = new ConfigInfoIp("6a5eabf50a0a115105dc6f8888d9cf",mockWebServer.url("/").toString(),"","","","");
        infoIpService = new InfoIpService(configInfoIp);
    }

   @Test
    void getInfoIp() throws Exception{
       ObjectMapper mapper = new ObjectMapper();
       LanguajeGetResponse languajeGetResponse = new LanguajeGetResponse("es");
       LocationGetResponse locationGetResponse = new LocationGetResponse( new LanguajeGetResponse[] {languajeGetResponse} );
       LocalizacionGetResponse localizacionGetResponse = new LocalizacionGetResponse("111.111.111.111","ipv4","SA","South America","CO","Colombia","DC","Bogota D.C.","Bogotá",locationGetResponse);

       MockResponse mockResponse = new MockResponse()
               .setBody(mapper.writeValueAsString(localizacionGetResponse))
               .addHeader("Content-Type", "application/json");
       mockWebServer.enqueue(mockResponse);

       LocalizacionGetResponse response = infoIpService.getInfoIp("111.111.111.111");
       Assertions.assertEquals(localizacionGetResponse.getIp(), response.getIp());
    }

}