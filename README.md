# respuesta-anti-fraude

Dada una Ip permite consultar información relacionada con el país desde el cual se realiza la consulta, siempre y cuando no se encuentre registrada en la Blacklist

## Installation
IntelliJ IDEA 2022.1.1 (Community Edition)
Java version 1.8.0_333

## Características

### Tecnologías:

* IntelliJ IDEA 2022.1.1 (Community Edition)
* Java version 1.8.0_333
* Spring Boot
* H2 Data Base
* SpringJPA

### Testing

* Junit
* Mockito
* MockMvc

### Requerimientos

* Java >= 8
* Spring Boot 2.7.0
* Gradle >= 7.4 

## Instalación

* Clonar repositorio
* Instalar dependencias
    ```gradle clean build --refresh-dependencies```

## Desplegar API

* Ejecutar CMD dentro de la carpeta del proyecto
* Ejecutar el comando gradle clean build
* Ejecutar el comando java -jar build/libs/respuesta-anti-fraude-1.0.0.jar

### Docker started

* ```docker build -t respuesta-anti-fraude .```
* ```docker run -it respuesta-anti-fraude```
* El API se ejecutará en http://localhost:8081/
* El API está documentado y se puede consumir a taves de http://localhost:8081/swagger-ui/index.html#/

## Testing y Coverage

* gradle test

## Documentacion API (SWAGGER)

* Desde el SWAGGER-UI se puede ver como consumir y que responde el API, a su vez se puede realizar consumo y pruebas a traves del mismo
    ```http://localhost:8081/swagger-ui/index.html#/```




