package com.mercadolibre.respuestaantifraude.Services;

import com.mercadolibre.respuestaantifraude.ConfigInfoIp;
import com.mercadolibre.respuestaantifraude.Response.LocalizacionGetResponse;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class InfoIpService {
    private ConfigInfoIp configProperties;

    public InfoIpService(ConfigInfoIp configProperties){ this.configProperties = configProperties; }

    @Cacheable(cacheNames = "InfoIp")
    public LocalizacionGetResponse getInfoIp(String ip){
        WebClient webClient = WebClient.create( configProperties.getUrlLocalicacion() );
        return webClient.get()
                .uri( uriBuilder ->  uriBuilder
                        .path("/{ip}")
                        .queryParam("access_key", configProperties.getKeyLocalizacion())
                        .build(ip))
                .retrieve()
                .bodyToMono(LocalizacionGetResponse.class)
                .block();
    }

}
