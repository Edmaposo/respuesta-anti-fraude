package com.mercadolibre.respuestaantifraude.Services;

import com.mercadolibre.respuestaantifraude.ConfigInfoIp;
import com.mercadolibre.respuestaantifraude.Response.PaisGetResponse;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class InfoPaisService {
    private ConfigInfoIp configProperties;

    public InfoPaisService(ConfigInfoIp configProperties){this.configProperties = configProperties; }

    @Cacheable(cacheNames = "InfoPais")
    public PaisGetResponse[] getInfoPais(String pais){
        WebClient webClient = WebClient.create( configProperties.getUrlInfoPais());
        return webClient.get()
                .uri( uriBuilder ->  uriBuilder
                        .path("/v2/name/{pais}")
                        .queryParam("access_key", configProperties.getKeyInfoPais())
                        .build(pais))
                .retrieve()
                .bodyToMono(PaisGetResponse[].class)
                .block();
    }
}
