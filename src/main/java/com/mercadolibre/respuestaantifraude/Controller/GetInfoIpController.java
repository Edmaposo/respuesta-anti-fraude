package com.mercadolibre.respuestaantifraude.Controller;

import com.mercadolibre.respuestaantifraude.Response.*;
import com.mercadolibre.respuestaantifraude.Entity.BlackList;
import com.mercadolibre.respuestaantifraude.Services.BlackListServices;
import com.mercadolibre.respuestaantifraude.Services.InfoIpService;
import com.mercadolibre.respuestaantifraude.Services.InfoMonedaService;
import com.mercadolibre.respuestaantifraude.Services.InfoPaisService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Currency;
import java.util.List;
import java.util.Locale;

@Controller
public class GetInfoIpController {
    private InfoIpService infoIpService;
    private LocalizacionGetResponse pais;
    private InfoPaisService infoPaisService;
    private BlackListServices blackListServices;
    private InfoMonedaService infoMonedaService;


    public GetInfoIpController(InfoIpService infoIpService,InfoPaisService infoPaisService,InfoMonedaService infoMonedaService,
                               BlackListServices blackListServices){
        this.infoIpService = infoIpService;
        this.infoPaisService = infoPaisService;
        this.blackListServices = blackListServices;
        this.infoMonedaService = infoMonedaService;
    }


    @Operation(summary = "Consultar IP")
    @ApiResponse(responseCode = "200", description = "Informacion relacionada con la IP",
    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = RespuestaAntifraudeResponse.class))})
    @ApiResponse(responseCode = "403", description = "Sin permisos para visualizar la información", content = @Content())
    @GetMapping("/get-info-ip/{ip}")
    @ResponseBody
    public ResponseEntity<RespuestaAntifraudeResponse> getInfoIP(@PathVariable("ip") String ip) {
        List<BlackList> listaIpExistentes = blackListServices.getBlackListByIp(ip);
        RespuestaAntifraudeResponse dto = new RespuestaAntifraudeResponse();

        if (listaIpExistentes.size() == 0) {
            LocalizacionGetResponse localizacionResponse = this.infoIpService.getInfoIp(ip);
            PaisGetResponse paisResponse = this.infoPaisService.getInfoPais(localizacionResponse.getCountry_name())[0];

            String codigoIdioma = localizacionResponse.getLocation().getLanguages()[0].getCode();
            String codigoISO = paisResponse.getAlpha3Code();
            Currency codigoMoneda = Currency.getInstance( new Locale(codigoIdioma,paisResponse.getAlpha2Code()));

            MonedaGetResponse monedaGetResponse = this.infoMonedaService.getInfoMoneda(codigoMoneda.toString());

            dto.setNombrePais(localizacionResponse.getCountry_name());
            dto.setCodigoIso(codigoISO);
            dto.setMoneda(codigoMoneda.toString());
            dto.setCotizacionMonedaEnDolares(monedaGetResponse.getResult());

            return new ResponseEntity<>(dto,HttpStatus.OK);
        }
        else throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Sin permisos para visualizar la información");
    }

    @Operation(summary = "Añadir IP a la lista negra")
    @ApiResponse(responseCode = "200", description = "La IP ha sigo añadida a la lista negra",
    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = BlackList.class))})
    @ApiResponse(responseCode = "404", description = "La IP ya se encuentra registrada", content = @Content())
    @PostMapping("/post-black-list")
    @ResponseBody
    public ResponseEntity<BlackList> postBlackList(@RequestBody @NotNull BlackLisPostResponse ip){
        List<BlackList> listaIpExistentes = blackListServices.getBlackListByIp(ip.getIp());

        if (listaIpExistentes.size() == 0) {
            BlackList ipAgregada = blackListServices.addBlackList(ip.getIp());
            return new ResponseEntity<>(ipAgregada, HttpStatus.OK);
        }
        else throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "La IP ya se encuentra registrada");
    }

}
