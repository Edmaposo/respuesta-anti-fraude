package com.mercadolibre.respuestaantifraude.Response;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class PaisGetResponseTest {

    private final PaisGetResponse paisGetResponse = new PaisGetResponse();

    @Test
    void getName() {
        paisGetResponse.setName("Colombia");
        assertEquals("Colombia", paisGetResponse.getName());
    }

    @Test
    void getAlpha2Code() {
        paisGetResponse.setAlpha2Code("CO");
        assertEquals("CO", paisGetResponse.getAlpha2Code());
    }

    @Test
    void getAlpha3Code() {
        paisGetResponse.setAlpha3Code("COL");
        assertEquals("COL", paisGetResponse.getAlpha3Code());
    }

    @Test
    void getCapital() {
        paisGetResponse.setCapital("Bogotá DC");
        assertEquals("Bogotá DC", paisGetResponse.getCapital());
    }

    @Test
    void getRegion() {
        paisGetResponse.setRegion("Américas");
        assertEquals("Américas", paisGetResponse.getRegion());
    }

    @Test
    void setName() {
        paisGetResponse.setName("Colombia");
        assertEquals("Colombia", paisGetResponse.getName());
    }

    @Test
    void setAlpha2Code() {
        paisGetResponse.setAlpha2Code("CO");
        assertEquals("CO", paisGetResponse.getAlpha2Code());
    }

    @Test
    void setAlpha3Code() {
        paisGetResponse.setAlpha3Code("COL");
        assertEquals("COL", paisGetResponse.getAlpha3Code());
    }

    @Test
    void setCapital() {
        paisGetResponse.setCapital("Bogotá DC");
        assertEquals("Bogotá DC", paisGetResponse.getCapital());
    }

    @Test
    void setRegion() {
        paisGetResponse.setRegion("Américas");
        assertEquals("Américas", paisGetResponse.getRegion());
    }

    @Test
    void builder() {
        PaisGetResponse response = PaisGetResponse.builder()
                .name("Colombia")
                .alpha2Code("CO")
                .alpha3Code("COL")
                .capital("Bogotá DC")
                .region("Américas")
                .build();
        assertEquals("Colombia", response.getName());
    }
}