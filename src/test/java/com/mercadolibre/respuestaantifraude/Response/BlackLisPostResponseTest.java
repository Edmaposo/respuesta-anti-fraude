package com.mercadolibre.respuestaantifraude.Response;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class BlackLisPostResponseTest {

    private final BlackLisPostResponse blackLisPostResponse = new BlackLisPostResponse();

    @Test
    void getIp() {
        blackLisPostResponse.setIp("111.111.111.111");
        assertEquals("111.111.111.111", blackLisPostResponse.getIp());
    }

    @Test
    void setIp() {
        blackLisPostResponse.setIp("111.111.111.111");
        assertEquals("111.111.111.111", blackLisPostResponse.getIp());
    }

    @Test
    void builder() {
        BlackLisPostResponse response = BlackLisPostResponse.builder()
                .ip("111.111.111.111")
                .build();
        assertEquals("111.111.111.111", response.getIp());
    }
}