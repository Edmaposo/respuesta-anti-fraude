package com.mercadolibre.respuestaantifraude;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class RespuestaAntiFraudeApplication {

	public static void main(String[] args) {
		SpringApplication.run(RespuestaAntiFraudeApplication.class, args);
	}

}
