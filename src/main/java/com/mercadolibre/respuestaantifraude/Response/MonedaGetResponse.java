package com.mercadolibre.respuestaantifraude.Response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MonedaGetResponse {
    private String result;

}
