package com.mercadolibre.respuestaantifraude.Services;

import com.mercadolibre.respuestaantifraude.Entity.BlackList;
import com.mercadolibre.respuestaantifraude.Repository.BlackLisRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;


@ExtendWith(MockitoExtension.class)
class BlackListServicesIntegrationTest {

    @InjectMocks
    BlackListServices blackListServices;
    @Mock
    BlackLisRepository Repository;


    @Test
    void addBlackList(){
        BlackList blackList = new BlackList(new Long("148776522737555"), "222.222.222.222");
        Mockito.when(Repository.save(Mockito.any())).thenReturn(blackList);
        BlackList response = blackListServices.addBlackList("222.222.222.222");
        Assertions.assertNotNull(response);
        Assertions.assertEquals("222.222.222.222", response.getIp());
    }

    @Test
    void getBlackListByIp() {
        BlackList blackList = new BlackList(new Long("148776522737000"), "111.111.111.111");

        Mockito.when(Repository.findByIp(blackList.getIp())).thenReturn(Arrays.asList(blackList));
        List<BlackList> lista = blackListServices.getBlackListByIp(blackList.getIp());
        Assertions.assertNotNull(lista);
        Assertions.assertEquals(1, lista.size());
    }

}