package com.mercadolibre.respuestaantifraude.Services;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercadolibre.respuestaantifraude.ConfigInfoIp;
import com.mercadolibre.respuestaantifraude.Response.PaisGetResponse;
import jdk.nashorn.internal.parser.JSONParser;
import mockwebserver3.MockResponse;
import mockwebserver3.MockWebServer;
import org.apache.logging.log4j.spi.ObjectThreadContextMap;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class InfoPaisServiceTest {

    @MockBean
    private InfoPaisService infoPaisService;
    private static MockWebServer mockWebServer;

    @BeforeAll
    static void setUp() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
    }

    @AfterAll
    static void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

    @BeforeEach
    void initialize() {
        ConfigInfoIp configInfoIp = new ConfigInfoIp("","","6a5eabf50a0a115105dc6f8888d9cf",mockWebServer.url("/").toString(),"","");
        infoPaisService = new InfoPaisService(configInfoIp);
    }


    @Test
    void getInfoPais()  throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        PaisGetResponse pais = new PaisGetResponse("Colombia","CO","COP","Bogotá","America");

        MockResponse mockResponse = new MockResponse()
                .setBody(mapper.writeValueAsString(new PaisGetResponse[]{pais}))
                .addHeader("Content-Type", "application/json");
        mockWebServer.enqueue(mockResponse);

        PaisGetResponse response = infoPaisService.getInfoPais("Colombia")[0];
        Assertions.assertEquals("Colombia", response.getName());
    }
}