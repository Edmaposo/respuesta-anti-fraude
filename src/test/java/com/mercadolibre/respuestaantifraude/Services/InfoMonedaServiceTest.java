package com.mercadolibre.respuestaantifraude.Services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercadolibre.respuestaantifraude.ConfigInfoIp;
import com.mercadolibre.respuestaantifraude.Response.LocalizacionGetResponse;
import com.mercadolibre.respuestaantifraude.Response.MonedaGetResponse;
import mockwebserver3.MockResponse;
import mockwebserver3.MockWebServer;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.IOException;

class InfoMonedaServiceTest {

    @MockBean
    private InfoMonedaService infoMonedaService;
    private static MockWebServer mockWebServer;

    @BeforeAll
    static void setUp() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
    }

    @AfterAll
    static void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

    @BeforeEach
    void initialize() {
        ConfigInfoIp configInfoIp = new ConfigInfoIp("","","","","6a5eabf50a0a115105dc6f8888d9cf",mockWebServer.url("/").toString());
        infoMonedaService = new InfoMonedaService(configInfoIp);
    }

    @Test
    void getInfoMoneda() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        MonedaGetResponse monedaGetResponse = new MonedaGetResponse("3922.5");

        MockResponse mockResponse = new MockResponse()
                .setBody(mapper.writeValueAsString(monedaGetResponse))
                .addHeader("Content-Type", "application/json");
        mockWebServer.enqueue(mockResponse);

        MonedaGetResponse response = infoMonedaService.getInfoMoneda("COP");
        Assertions.assertEquals("3922.5", response.getResult());
    }
}