package com.mercadolibre.respuestaantifraude.Response;

import com.mercadolibre.respuestaantifraude.Entity.BlackList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LanguajeGetResponseTest {

    private final LanguajeGetResponse languajeGetResponse = new LanguajeGetResponse();

    @Test
    void getCode() {
        languajeGetResponse.setCode("es");
        assertEquals("es", languajeGetResponse.getCode());
    }

    @Test
    void setCode() {
        languajeGetResponse.setCode("es");
        assertEquals("es", languajeGetResponse.getCode());
    }

    @Test
    void builder() {
        LanguajeGetResponse response = LanguajeGetResponse.builder()
                .code("es")
                .build();
        assertEquals("es", response.getCode());
    }
}