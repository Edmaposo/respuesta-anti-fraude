package com.mercadolibre.respuestaantifraude.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercadolibre.respuestaantifraude.Response.*;
import com.mercadolibre.respuestaantifraude.Services.BlackListServices;
import com.mercadolibre.respuestaantifraude.Services.InfoIpService;
import com.mercadolibre.respuestaantifraude.Services.InfoMonedaService;
import com.mercadolibre.respuestaantifraude.Services.InfoPaisService;
import com.mercadolibre.respuestaantifraude.Utils.UtilTest;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Collections;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@WebMvcTest(GetInfoIpController.class)
public class ControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private InfoIpService infoIpService;
    @MockBean
    private InfoPaisService infoPaisService;
    @MockBean
    private BlackListServices blackListServices;
    @MockBean
    private InfoMonedaService infoMonedaService;
    @MockBean
    GetInfoIpController getInfoIpController;
    ObjectMapper mapper = new ObjectMapper();


    @Test
    void getInfoIPSuccess() throws Exception {
        Mockito.when(blackListServices.getBlackListByIp("111.111.111.111")).thenReturn(Collections.singletonList(UtilTest.getValidBlackListRequest()));
        Mockito.when(infoIpService.getInfoIp(Mockito.any())).thenReturn(UtilTest.getLocalizacionValidRequest());
        Mockito.when(infoPaisService.getInfoPais(Mockito.any())).thenReturn(new PaisGetResponse[] {UtilTest.getPaisValidRequest()});
        Mockito.when(infoMonedaService.getInfoMoneda(Mockito.any())).thenReturn(UtilTest.getMonedaValidRequest());

        mockMvc.perform(
                        MockMvcRequestBuilders.get("/get-info-ip/{ip}","111.111.111.111")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.writeValueAsString(UtilTest.getValidRespuestaRequest())))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    /*@Test
    void getInfoIPForbidden() throws Exception {
        //ObjectMapper mapper = new ObjectMapper();

        Mockito.when(blackListServices.getBlackListByIp("111.111.111.111")).thenReturn(Collections.singletonList(UtilTest.getValidBlackListRequest()));
        //Mockito.when(infoIpService.getInfoIp(Mockito.any())).thenReturn(UtilTest.getLocalizacionValidRequest());
        //Mockito.when(infoPaisService.getInfoPais(Mockito.any())).thenReturn(new PaisGetResponse[] {UtilTest.getPaisValidRequest()});
        //Mockito.when(infoMonedaService.getInfoMoneda(Mockito.any())).thenReturn(UtilTest.getMonedaValidRequest());

        mockMvc.perform(
                        MockMvcRequestBuilders.get("/get-info-ip/{ip}","111.111.111.111")
                                //.contentType(MediaType.APPLICATION_JSON)
//                                /.content(mapper.writeValueAsString(UtilTest.getValidRespuestaRequest())))
                )
                .andDo(print())
                .andExpect(status().isForbidden())
                .andReturn();
    }*/

    @Test
    void addBlackListSuccess() throws Exception{
        Mockito.when(blackListServices.getBlackListByIp("111.111.111.111")).thenReturn(new ArrayList<>());

        mockMvc.perform(MockMvcRequestBuilders.post("/post-black-list")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(UtilTest.getValidBlackListRequest())))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }
}
