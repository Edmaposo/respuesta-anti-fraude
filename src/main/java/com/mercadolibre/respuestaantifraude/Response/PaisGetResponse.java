package com.mercadolibre.respuestaantifraude.Response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaisGetResponse {
    private String name;
    private String alpha2Code;
    private String alpha3Code;
    private String capital;
    private String region;
}
