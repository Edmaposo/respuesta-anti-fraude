package com.mercadolibre.respuestaantifraude.Repository;

import com.mercadolibre.respuestaantifraude.Entity.BlackList;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BlackLisRepository extends JpaRepository<BlackList,Long> {

    List<BlackList> findByIp(String ip);
}
