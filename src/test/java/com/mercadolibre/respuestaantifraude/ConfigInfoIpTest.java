package com.mercadolibre.respuestaantifraude;

import lombok.Builder;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConfigInfoIpTest {

    private final ConfigInfoIp configInfoIp = new ConfigInfoIp();

    @Test
    void getKeyLocalizacion() {
        configInfoIp.setKeyLocalizacion("4587542155555");
        assertEquals("4587542155555", configInfoIp.getKeyLocalizacion());
    }

    @Test
    void getUrlLocalicacion() {
        configInfoIp.setUrlLocalicacion("http://prueba.com");
        assertEquals("http://prueba.com", configInfoIp.getUrlLocalicacion());
    }

    @Test
    void getKeyInfoPais() {
        configInfoIp.setKeyInfoPais("4587542155555");
        assertEquals("4587542155555", configInfoIp.getKeyInfoPais());
    }

    @Test
    void getUrlInfoPais() {
        configInfoIp.setUrlInfoPais("http://prueba.com");
        assertEquals("http://prueba.com", configInfoIp.getUrlInfoPais());
    }

    @Test
    void getKeyInfoMonedas() {
        configInfoIp.setKeyInfoMonedas("4587542155555");
        assertEquals("4587542155555", configInfoIp.getKeyInfoMonedas());
    }

    @Test
    void getUrlInfoMonedas() {
        configInfoIp.setUrlInfoMonedas("http://prueba.com");
        assertEquals("http://prueba.com", configInfoIp.getUrlInfoMonedas());
    }

    @Test
    void setKeyLocalizacion() {
        configInfoIp.setKeyLocalizacion("4587542155555");
        assertEquals("4587542155555",  configInfoIp.getKeyLocalizacion());
    }

    @Test
    void setUrlLocalicacion() {
        configInfoIp.setUrlLocalicacion("http://prueba.com");
        assertEquals("http://prueba.com",  configInfoIp.getUrlLocalicacion());
    }

    @Test
    void setKeyInfoPais() {
        configInfoIp.setKeyInfoPais("4587542155555");
        assertEquals("4587542155555",  configInfoIp.getKeyInfoPais());
    }

    @Test
    void setUrlInfoPais() {
        configInfoIp.setUrlInfoPais("http://prueba.com");
        assertEquals("http://prueba.com",  configInfoIp.getUrlInfoPais());
    }

    @Test
    void setKeyInfoMonedas() {
        configInfoIp.setKeyInfoMonedas("4587542155555");
        assertEquals("4587542155555",  configInfoIp.getKeyInfoMonedas());
    }

    @Test
    void setUrlInfoMonedas() {
        configInfoIp.setUrlInfoMonedas("http://prueba.com");
        assertEquals("http://prueba.com",  configInfoIp.getUrlInfoMonedas());
    }

    @Test
    void Builder(){
        ConfigInfoIp response = ConfigInfoIp.builder()
                .keyLocalizacion("4587542155555")
                .urlLocalicacion("http://prueba.com")
                .keyInfoMonedas("45875421")
                .urlInfoMonedas("http://prueba.com")
                .keyInfoPais("45875421555")
                .urlInfoPais("http://prueba.com")
                .build();
        assertEquals("4587542155555", response.getKeyLocalizacion());
    }
}