package com.mercadolibre.respuestaantifraude.Utils;

import com.mercadolibre.respuestaantifraude.Entity.BlackList;
import com.mercadolibre.respuestaantifraude.Response.*;

public class UtilTest {

    public static RespuestaAntifraudeResponse getValidRespuestaRequest(){
        RespuestaAntifraudeResponse respuestaAntifraudeResponse = new RespuestaAntifraudeResponse();
        respuestaAntifraudeResponse.setNombrePais("Colombia");
        respuestaAntifraudeResponse.setCodigoIso("COL");
        respuestaAntifraudeResponse.setMoneda("COP");
        respuestaAntifraudeResponse.setCotizacionMonedaEnDolares("3922.5");

        return respuestaAntifraudeResponse;
    }

    public static BlackList getValidBlackListRequest(){
        BlackList blackList = new BlackList();
        blackList.setId(6656L);
        blackList.setIp("111.111.111.111");

        return blackList;
    }

    public static LocalizacionGetResponse getLocalizacionValidRequest(){
        LanguajeGetResponse languajeGetResponse = new LanguajeGetResponse();
        languajeGetResponse.setCode("es");

        LocationGetResponse locationGetResponse = new LocationGetResponse( );
        locationGetResponse.setLanguages(new LanguajeGetResponse[] {languajeGetResponse});

        LocalizacionGetResponse localizacionGetResponse = new LocalizacionGetResponse();
        localizacionGetResponse.setIp("111.111.111.111");
        localizacionGetResponse.setType("ipv4");
        localizacionGetResponse.setContinent_code("SA");
        localizacionGetResponse.setContinent_name("South America");
        localizacionGetResponse.setCountry_code("CO");
        localizacionGetResponse.setContinent_name("Colombia");
        localizacionGetResponse.setRegion_code("DC");
        localizacionGetResponse.setRegion_name("Bogota D.C.");
        localizacionGetResponse.setCity("Bogotá");
        localizacionGetResponse.setLocation(locationGetResponse);

        return localizacionGetResponse;
    }

    public static PaisGetResponse getPaisValidRequest (){
        PaisGetResponse paisGetResponse = new PaisGetResponse();
        paisGetResponse.setName("Colombia");
        paisGetResponse.setAlpha2Code("CO");
        paisGetResponse.setAlpha3Code("COL");
        paisGetResponse.setCapital("Bogotá");
        paisGetResponse.setRegion("Américas");

        return paisGetResponse;
    }

    public static MonedaGetResponse getMonedaValidRequest(){
        MonedaGetResponse monedaGetResponse = new MonedaGetResponse();
        monedaGetResponse.setResult("3922.5");

        return monedaGetResponse;
    }

    public static BlackLisPostResponse getBlackListRequest(){
        BlackLisPostResponse blackLisPostResponse = new BlackLisPostResponse();
        blackLisPostResponse.setIp("111.111.111.111");

        return blackLisPostResponse;
    }

}
