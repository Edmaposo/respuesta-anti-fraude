package com.mercadolibre.respuestaantifraude.Response;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class RespuestaAntifraudeResponseTest {

    private final RespuestaAntifraudeResponse respuestaAntifraudeResponse = new RespuestaAntifraudeResponse();

    @Test
    void getNombrePais() {
        respuestaAntifraudeResponse.setNombrePais("Colombia");
        assertEquals("Colombia", respuestaAntifraudeResponse.getNombrePais());
    }

    @Test
    void getCodigoIso() {
        respuestaAntifraudeResponse.setCodigoIso("COL");
        assertEquals("COL", respuestaAntifraudeResponse.getCodigoIso());
    }

    @Test
    void getMoneda() {
        respuestaAntifraudeResponse.setMoneda("COP");
        assertEquals("COP", respuestaAntifraudeResponse.getMoneda());
    }

    @Test
    void getCotizacionMonedaEnDolares() {
        respuestaAntifraudeResponse.setCotizacionMonedaEnDolares("3500");
        assertEquals("3500", respuestaAntifraudeResponse.getCotizacionMonedaEnDolares());
    }

    @Test
    void setNombrePais() {
        respuestaAntifraudeResponse.setNombrePais("Colombia");
        assertSame("Colombia", respuestaAntifraudeResponse.getNombrePais());
    }

    @Test
    void setCodigoIso() {
        respuestaAntifraudeResponse.setCodigoIso("COL");
        assertSame("COL", respuestaAntifraudeResponse.getCodigoIso());
    }

    @Test
    void setMoneda() {
        respuestaAntifraudeResponse.setMoneda("COP");
        assertSame("COP", respuestaAntifraudeResponse.getMoneda());
    }

    @Test
    void setCotizacionMonedaEnDolares() {
        respuestaAntifraudeResponse.setCotizacionMonedaEnDolares("3500");
        assertSame("3500", respuestaAntifraudeResponse.getCotizacionMonedaEnDolares());
    }

    @Test
    void builder() {
        RespuestaAntifraudeResponse response = RespuestaAntifraudeResponse.builder()
                .nombrePais("Colombia")
                .codigoIso("COL")
                .moneda("COP")
                .cotizacionMonedaEnDolares("3500")
                .build();

        assertEquals("Colombia", response.getNombrePais());
    }
}