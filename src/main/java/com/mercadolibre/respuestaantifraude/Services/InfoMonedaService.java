package com.mercadolibre.respuestaantifraude.Services;

import com.mercadolibre.respuestaantifraude.ConfigInfoIp;
import com.mercadolibre.respuestaantifraude.Response.LocalizacionGetResponse;
import com.mercadolibre.respuestaantifraude.Response.MonedaGetResponse;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class InfoMonedaService {

    private ConfigInfoIp configProperties;

    public InfoMonedaService(ConfigInfoIp configProperties){this.configProperties = configProperties; }

    @Cacheable(cacheNames = "InfoMoneda")
    public MonedaGetResponse getInfoMoneda(String base){
        WebClient webClient = WebClient.create( configProperties.getUrlInfoMonedas());

        MonedaGetResponse response = webClient.get()
                .uri( uriBuilder ->  uriBuilder
                        .path("/fixer/convert")
                        .queryParam("apikey", configProperties.getKeyInfoMonedas())
                        .queryParam("from", "USD")
                        .queryParam("to", base)
                        .queryParam("amount", 1)
                        .build())
                .retrieve()
                .bodyToMono(MonedaGetResponse.class)
                .block();

        return response;
    }

}
