FROM openjdk:8-jdk-alpine
EXPOSE 8081
COPY build/libs/respuesta-anti-fraude-0.0.1-SNAPSHOT.jar respuesta-anti-fraude-1.0.0.jar
ENTRYPOINT ["java","-jar","respuesta-anti-fraude-1.0.0.jar"]